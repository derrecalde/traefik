# TRAEFIK

Manager Docker container, firewall & ssl manager.   
[Doc](https://blog.silarhi.fr/docker-compose-traefik-https/)

## Create files 
- /etc/traefik/ ```docker-compose.yml```   
- /etc/traefik/ ```acme.json``` & ```chmod 600 acme.json```    
- /etc/traefik.d/ ```middelwares.toml```    

### Check the existing networks  
```docker networks ls```

### Create a docker network named traefik_default
```docker networks create traefik_default```   

### Start traefik from /etc/traefik/ (take care no container are up before)
```docker-compose up -d```

### Start your project
```docker-compose up -d```

### Check treafik ssl error
```docker logs proxy_1 | grep "level=error" | grep "acme"```

Delete logs
```truncate -s 0 $(docker inspect --format='{{.LogPath}}' <container_name_traefik_or_id>)```

